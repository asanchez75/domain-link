<?php

/**
 * @file
 * Definition of views_handler_field_url.
 */

/**
 * Field handler to provide simple renderer that turns a URL into a clickable link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_domain_link_node_path extends views_handler_field_node_path {
  function option_definition() {
    $options = parent::option_definition();
    $options['display_link_with_subdomain'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    $form['display_link_with_subdomain'] = array(
      '#title' => t('Display absolute link with subdomain'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_link_with_subdomain']),
    );
    parent::options_form($form, $form_state);
  }

  function render($values) {
    global $language;
    global $base_url;

    $nid = $this->get_value($values, 'nid');
    if (($this->options['display_link_with_subdomain']) == TRUE && ($this->options['absolute'] == FALSE)) {
      $url = drupal_get_path_alias('node/' . $nid, $language->language);
      $url = trim($url, '/');
      // not include language if uses english
      return $base_url . '/' . (($language->language == 'en') ?  '' :  $language->language) . '/' . $url;
    }
    else {
      return url("node/$nid", array('absolute' => $this->options['absolute']));
    }

  }
}
