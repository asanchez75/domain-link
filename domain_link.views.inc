<?php

/**
 * Implementation of hook_views_data_alter().
 */
function domain_link_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    foreach ($config as $item => $item_config) {
      if (isset($item_config['field']['handler']) && $item_config['field']['handler'] == 'views_handler_field_node_path') {
        $data[$table][$item]['field']['handler'] = 'views_handler_field_domain_link_node_path';
      }
    }
  }

  return $data;
}


